const tagSchema = `
type Tag {
    id: Int!,
    name: String,
}

`

module.exports = tagSchema
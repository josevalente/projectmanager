const projectSchema = `
type Project {
    id: ID!,
    user_id: String,
    name: String,
    description: String,
    state: String,
    date: String,
    repository: String
}`

module.exports = projectSchema
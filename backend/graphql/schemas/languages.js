const languageSchema = `
type Language {
    id: Int!,
    name: String,
    imagePath: String
}

`

module.exports = languageSchema
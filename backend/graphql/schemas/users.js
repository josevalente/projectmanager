const userSchema = `
type User {
    id: ID!,
    name: String,
    email: String,
    password: String,
    profile_description: String,
    profile_image: String,
    profile_public: Boolean,
    createdAt: String,
    updatedAt: String,
}

input createUserInput {
    name: String!,
    email: String!,
    password: String!,
}

input updateUserInput {
    id: String!,
    name: String,
    profile_description: String,
    profile_image: String,
    profile_public: Boolean,

}
`

module.exports = userSchema
const { getProjectTags } = require('../../dataBase/db')

let tagsResolver = {
    getProjectTags(obj){
        return getProjectTags(obj.id, obj.limit ?? null)
    }

}

module.exports = tagsResolver
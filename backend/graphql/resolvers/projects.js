const { countUserProjects, getUserProjects} = require('../../dataBase/db')

let projectsResolver = {
    countUserProjects(obj){
        return countUserProjects(obj.id)
    },
    getUserProjects(obj){
        return getUserProjects(obj.id, obj.offset ?? 0, obj.limit ?? null)
    }
}

module.exports = projectsResolver
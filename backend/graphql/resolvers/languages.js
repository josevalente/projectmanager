const { getLanguages, getLanguageById, getLanguageByName, countUserLanguages, getProjectLanguages } = require("../../dataBase/db");

let languagesResolver = {
    getAllLanguages(){
        return getLanguages();
    },
    getLanguageById (obj){
        return getLanguageById( obj.id);
    },
    getLanguageByName(obj ){
        return getLanguageByName( obj.name);
    },
    countUserLanguages(obj){
        return countUserLanguages(obj.id)
    },
    getProjectLanguages(obj){
        return getProjectLanguages(obj.id, obj.limit ?? null)
    }
}

module.exports = languagesResolver;
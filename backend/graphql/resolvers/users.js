const { getUserById, createUser, getUserByEmail, deleteUser, checkUserCredentials, updateUser } = require('../../dataBase/db')

let usersResolver = {
    getUserById(obj){
        return getUserById(obj.id);
    },
    createUser(root, args, context, info){
        return createUser(root.user)
    },
    deleteUser(obj) {
        return deleteUser(obj.id)
    },
    checkUserCredentials(root, args, context, info){
        return checkUserCredentials( root.email, root.password)
    },
    updateUser(root, args, context, info){
        return updateUser(root.userUpdate)
    },
    getUserByEmail(obj) {
        return getUserByEmail(obj.email);
    }

}

module.exports = usersResolver;
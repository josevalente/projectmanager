const { Sequelize } = require('sequelize')
const { sequelize } = require('./db')

const tableLanguages = require('./languages')
const tableProjects = require('./projects')

const tableProjectsLanguages = sequelize.define('project_languages', {
    id: {
        type: Sequelize.INTEGER(4),
        primaryKey: true,
        allowNull: false,
    }
}, {timestamps: false})



tableProjects.belongsToMany(tableLanguages, {
    through: tableProjectsLanguages,
    foreignKey: 'languageId'
})

tableLanguages.belongsToMany(tableProjects, {
    through: tableProjectsLanguages,
    foreignKey: 'projectId'
})

tableProjectsLanguages.sync()

module.exports = tableProjectsLanguages;
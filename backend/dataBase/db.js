// Checks if sequelize connection with database was successful
async function checkDbConnection(){
    try{
        await sequelize.authenticate();
        console.log("Connection successful")
    }catch( err){ console.log(err)}
}

// Languages table queries

async function getLanguages () {
    try{
        return await tableLanguages.findAll({raw: true});
    }catch ( err){
        console.log(err);
    }
}

async function getLanguageById ( idToFind) {
    try {
        return await tableLanguages.findOne( {raw: true, where: { id: idToFind}})
    } catch (err) {
        console.log(err)
    }
}

async function getLanguageByName ( nameToFind) {
    try {
        return await tableLanguages.findAll( {raw: true, where : {  name: { [Op.like]: nameToFind } }})
    } catch (err) {
        console.log(err);
    }
}

async function getProjectLanguages ( projectId, limit = null){
    try {
        
        let languages = await tableProjectsLanguages.findAll({
            attributes: ['languageId'], 
            where: { projectId: projectId}, 
            limit: limit, 
            raw: true
        })
        if ( languages.length <= 0)
            return null

        let languagesId = []
        for( let language of languages)
            languagesId.push( language.languageId)

        return await tableLanguages.findAll({
            where : {
                id: {
                    [Op.or]: languagesId
                }
            },
            raw: true
        })


    } catch (err) {
        console.log(err)
    }
}

async function getProjectTags ( projectId, limit = null){
    try {
        
        let tags = await tableProjectsTags.findAll({
            attributes: ['tagId'], 
            where: { projectId: projectId}, 
            limit: limit, 
            raw: true
        })
        if ( tags.length <= 0)
            return null

        let tagsId = []
        for( let tag of tags)
            tagsId.push( tag.tagId)

        return await tableTags.findAll({
            where : {
                id: {
                    [Op.or]: tagsId
                }
            },
            raw: true
        })


    } catch (err) {
        console.log(err)
    }
}

// Users table queries
async function getUserByEmail(email) {
    try {
        return await tableUsers.findOne( {raw: true, where: {email: email}} )
    } catch (err) {
        console.log(err);
    }
}


async function getUserById( idToFind){
    try {
        let user =  await tableUsers.findOne( {raw: true, where: {id: idToFind}})
        userDateTimeTreatment( user)
        return user
    } catch (err) {
        console.log(err)   
    }
}

async function createUser( userToCreate){
    try{
        const hashedPassword = await hashPassword( userToCreate.password);

        let userCreated = await tableUsers.create({
            name: userToCreate.name,
            email: userToCreate.email,
            password: hashedPassword
        }, { raw: true})
        //Since graphql does'nt have a native date/dateTime type, we declare it as a string in graphql
        //and transform the dateTime into a string with toISOString
        userDateTimeTreatment( userCreated.dataValues)

        return userCreated;
    }catch( err){
        console.log( err)
    }

}

async function updateUser( userUpdate){
    try {
        let obj = {}
        for ( key in userUpdate)
            if ( key != "id")
                obj[key] = userUpdate[key]    
                    
        await tableUsers.update(obj, {
            where: {id: userUpdate.id}
        })


        return await getUserById( userUpdate.id)
        
    } catch (err) {
        console.log(err)
    }
}

async function deleteUser(id) {
    try {
        return await tableUsers.destroy({
            where: {id: id}
        }) > 0;
    } catch (error) {
        console.log(error);
    }
}

// checks user credentials, return values: 
// user.id = -2 if user with that email is not found
// user.id = -1 if password is not correct
// User object if both email and password are correct
async function checkUserCredentials( email, password){
    try {
        let user = await tableUsers.findOne({raw: true, where: {email: email }})
        
        if ( user === null)
        return user = { id: -2}
        
        if ( await comparePassword( password, user.password) ){
            userDateTimeTreatment(user)
            return user;
        }
        
        return user = { id: -1 };
        
    } catch (err) {
        console.log(err)
    }
}

async function countUserProjects( id){
    try {
        return await tableProjects.count( { where: { user_id: id}})
    } catch (err) {
        console.log(err)
    }
}

async function getUserProjects(id, offset = 0, limit = null){
    try {
        return await tableProjects.findAll({
            where: { user_id: id},
            offset: offset,
            limit: limit,
            raw: true
        })
    } catch (err) {
        console.log(err)
    }
}

async function countUserLanguages(id){
    try {
        let userProjects = await getUserProjects(id)
        let userProjectsId = []

        userProjects.forEach((item) => {
            userProjectsId.push(item.id)
        })
        
        return await tableProjectsLanguages.count({
            distinct: true,
            col: 'languageId',
            where: { projectId: userProjectsId },
        })
        
    } catch (err) {
        console.log(err)
    }
}

function userDateTimeTreatment( user){
    user.createdAt = user.createdAt.toISOString() 
    user.updatedAt = user.updatedAt.toISOString()
}

// password hashing
async function hashPassword( password, saltRounds = 10){
    try {
        const salt = await bcrypt.genSalt(saltRounds)
        return await bcrypt.hash(password, salt)
    } catch (err) {
        console.log(err)
    }
}

async function comparePassword( password, hashedPassword){
    try {
        return await bcrypt.compare(password, hashedPassword)  
    } catch (err) {
        console.log( err)
    }
}

require('dotenv/config');

const bcrypt= require('bcrypt');
const {Sequelize, Op} = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_SECRET, {
    dialect: "mysql",
    port: 3306,
})

checkDbConnection()

module.exports = { 
    getLanguages,
    getLanguageById,
    getLanguageByName,
    countUserLanguages,
    getProjectLanguages,

    getProjectTags,
    

    getUserByEmail,
    deleteUser,
    getUserById,
    createUser,
    checkUserCredentials,
    updateUser,

    countUserProjects,
    getUserProjects,
    
    sequelize,
};

const tableLanguages = require('./languages')
const tableUsers = require('./users')
const tableProjects = require('./projects')
const tableProjectsLanguages = require('./projectLanguages')
const tableTags = require('./tags')
const tableProjectsTags = require('./projectTags')
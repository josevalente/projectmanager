const {Sequelize} = require('sequelize');
const {sequelize} = require('./db');

const tableTags = sequelize.define('tag', {
    id:{
        type: Sequelize.INTEGER(4),
        primaryKey: true,
        allowNull: true
    },
    name: Sequelize.STRING(128),
    
}, {timestamps: false});
tableTags.sync();

module.exports = tableTags;
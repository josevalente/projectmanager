const { Sequelize } = require('sequelize')
const { sequelize } = require('./db')

const tableTags = require('./tags')
const tableProjects = require('./projects')

const tableProjectsTags = sequelize.define('project_tags', {
    id: {
        type: Sequelize.INTEGER(4),
        primaryKey: true,
        allowNull: false,
    }
}, {timestamps: false})



tableProjects.belongsToMany(tableTags, {
    through: tableProjectsTags,
    foreignKey: 'tagId'
})

tableTags.belongsToMany(tableProjects, {
    through: tableProjectsTags,
    foreignKey: 'projectId'
})

tableProjectsTags.sync()

module.exports = tableProjectsTags;
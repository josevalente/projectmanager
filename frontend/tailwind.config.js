// tailwind.config.js
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        primary1: 'var(--color-primary1)',
        primary2: 'var(--color-primary2)',
        secondary1: 'var(--color-secondary1)',
        secondary2: 'var(--color-secondary2)',
        nearWhite: 'var(--color-nearWhite)',
      },
      fontFamily: {
        Ubuntu: ['Ubuntu', 'sans-serif']
      }
    },
    
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
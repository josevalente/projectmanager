import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/sessions',
        component: () => import('../views/sessionView.vue'),
        children: [
            {
                name: 'login',
                path: '',
                component: () => import('../views/loginPage.vue'),
                alias: ['login']
            },
            {
                name: 'signup',
                path: 'signup',
                component: () => import('../views/signupPage.vue')
            }
        ]
    },
    {
        path: '/users/:userId/',
        component: () => import('../views/profileProjectsView.vue'),
        children: [
            {
                name: 'user',
                path: '',
                components: {
                    default: () => import('../views/userProjects.vue'),//todo: change this line
                    navBar: () => import('../components/NavBar/navBar.vue'),
                    profile: () => import('../components/Profile/profilePack.vue')
                },
                alias: ['/users/:userId/profile'],

            },
            
            
        ]        
    },
    {
        
        path: '/myProfile',
        component: () => import('../views/myProfilePage.vue'),
        children: [
            {
                name: 'myProfile',
                path: '',
                components: {
                    default: () => import('../views/manageProfileView.vue'),
                    navBar: () => import('../components/NavBar/navBar.vue'),
                    
                }
            }
        ]
    }
    
]

const router  = createRouter( { history: createWebHistory(import.meta.url.BASE_URL), routes: routes})


export default router
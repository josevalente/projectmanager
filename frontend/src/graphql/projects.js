import gql from "graphql-tag"

const COUNT_USER_PROJECTS = id => gql`
query {
    countUserProjects(id : "${id}")
}
`

const GET_USER_PROJECTS = ( id, offset = 0, limit= null) => gql`
query {
    getUserProjects(id: "${id}", offset: ${offset}, limit: ${limit}){
        id
        user_id
        name
        description
        state
        date
        repository  
    }
}
`

export default {

    COUNT_USER_PROJECTS,
    GET_USER_PROJECTS,
}

import gql from 'graphql-tag'

const GET_PROJECT_TAGS = ( id, limit = null) => gql`
query {
  getProjectTags(id: "${id}", limit: ${limit}){
    id
    name
  }

}
`

export default {
  GET_PROJECT_TAGS,
}
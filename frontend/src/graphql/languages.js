import gql from 'graphql-tag'

const GET_ALL_LANGUAGES = gql`
query { 
  getAllLanguages {   
    id
    name
    imagePath
  }
}
`

const GET_PROJECT_LANGUAGES = ( id, limit = null) => gql`
query {
  getProjectLanguages(id: "${id}", limit: ${limit}){
    name
    imagePath
  }

}
`

const COUNT_USER_LANGUAGES = id => gql`
query{
  countUserLanguages(id: "${id}")
}
`

export default {
  GET_ALL_LANGUAGES,
  GET_PROJECT_LANGUAGES,
  COUNT_USER_LANGUAGES
}
import gql from 'graphql-tag'

const CHECK_USER_CREDENTIALS = (email, password) => gql`
query {
    checkUserCredentials (email: "${email}", password: "${password}"){
        id
        name
        email
        password
        profile_description
        profile_image
        profile_public
        createdAt
        updatedAt
    }
}
`

const GET_USER_BY_ID = (id) => gql`
query {
    getUserById(id: "${id}"){
        id
        name
        email
        password
        profile_image
        profile_description
        profile_public
        createdAt
        updatedAt
    }
}
`

const GET_USER_BY_EMAIL = (email) => gql`
query {
    getUserByEmail(email: "${email}"){
        email
    }
}
`

const CREATE_USER = user => gql`
mutation {
    createUser(user: { name: "${user.name}", email: "${user.email}", password: "${user.password}" } ) {
        id
        name
        email
        password
        profile_description
        profile_image
        profile_public
        createdAt
        updatedAt 
    }
}

`

const UPDATE_USER = userUpdate => gql`
mutation {
    updateUser(userUpdate: {
         id: "${userUpdate.id}",
         name: "${userUpdate.name}",
         profile_description: "${userUpdate.profile_description}",
         profile_image: ${userUpdate.profile_image},
         profile_public: ${userUpdate.profile_public},
         } ) {
        id
        name
        email
        password
        profile_description
        profile_image
        profile_public
        createdAt
        updatedAt 
    }
}

`

const DELETE_USER = userDelete => gql`
mutation {
    deleteUser(id: "${userDelete.id}")
}

`

export default { 
    CHECK_USER_CREDENTIALS,
    GET_USER_BY_ID,
    GET_USER_BY_EMAIL,
    CREATE_USER,
    UPDATE_USER,
    DELETE_USER
}

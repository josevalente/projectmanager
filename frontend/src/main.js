import { createApp, provide, h  } from 'vue'
import App from './App.vue'
import './index.css' // tailwind file

//apollo client imports
import { apolloClient } from './apolloClient'
import { DefaultApolloClient } from '@vue/apollo-composable'

//font awesome imports
import {library} from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
import { 
  faPlus,
  faExclamationCircle,
  faEye, 
  faEyeSlash, 
  faSpinner,
  faRightToBracket,
  faRightFromBracket, 
  faUser,
  faAngleDown,
  faFloppyDisk,
  faTrash,
  faCheck,
  faCircleInfo,
} from '@fortawesome/free-solid-svg-icons'

import  router  from './routes/router'

library.add(faExclamationCircle)
library.add(faPlus);
library.add(faEye);
library.add(faEyeSlash);
library.add(faSpinner);
library.add(faRightToBracket)
library.add(faRightFromBracket)
library.add(faUser);
library.add(faAngleDown);
library.add(faFloppyDisk)
library.add(faTrash)
library.add(faCheck)
library.add(faCircleInfo)

const app = createApp({
    setup () {
      provide(DefaultApolloClient, apolloClient)
    },
    
    render: () => h(App),
})

app.mixin({
  methods: {
    createSession (user, rememberMe = false){
      
      window.localStorage.setItem('name', user.name)
      window.localStorage.setItem('email', user.email)
      window.localStorage.setItem('id', user.id)
      if ( !rememberMe ){
        let d = new Date();
        d.setDate( d.getDate() + 1)
        window.localStorage.setItem('expirationDate', d)
        
      }
    },
    getSession (){
      let expirationDate = null
      
      if ( window.localStorage.getItem('expirationDate') )
        expirationDate = new Date(window.localStorage.getItem('expirationDate'));

      if( expirationDate && Date.now() >= expirationDate.getTime() ){
          this.deleteSession()
      }
        
      return window.localStorage
    },
    deleteSession(){
      window.localStorage.clear()
    }
  }
})

app.use(router)

//create the component from FontAwesomeIcon as "font-awesome-icon"
app.component("font-awesome-icon", FontAwesomeIcon)

app.mount('#app')    

